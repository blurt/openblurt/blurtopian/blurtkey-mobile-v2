## Privacy Policy
## Effective date: June 30, 2022

Blurtopian ("us", "we", or "our") provides and operates the Blurtkey Mobile App (the "Service").

This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data.

### Information Collection And Use
The Blurtkey Mobile App does not collect any personal data of any kind. Account names and private keys entered into the extension are stored locally, are never transmitted anywhere, and are securely encrypted using the user-chosen password.

### Usage Data
The Blurtkey Mobile App collects encrypted navigation data in an anonymous fashion, through an encrypted communication channel. These information will never be given or sold to a third-party.

### Tracking & Cookies Data
The Service does not perform any tracking or store any cookies on users' devices.

### Transfer Of Data
No data stored within the Service is ever transfered or transmitted off of the user's device at any time or for any reason.

### Security Of Data
All data input into the Service is stored locally and securely encrypted using the password specified by the user and its biometrics whenever available.

### Changes To This Privacy Policy
We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.

We will let you know via a prominent notice on our Service, prior to the change becoming effective and update the "effective date" at the top of this Privacy Policy.

You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.

### Contact Us
If you have any questions about this Privacy Policy, please contact us:

By email: blurtopian@gmail.com
