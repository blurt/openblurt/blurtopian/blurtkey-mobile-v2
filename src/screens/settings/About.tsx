import Clipboard from '@react-native-community/clipboard';
import Background from 'components/ui/Background';
import Separator from 'components/ui/Separator';
import useLockedPortrait from 'hooks/useLockedPortrait';
import { AboutNavigation } from 'navigators/MainDrawer.types';
import React, { useState } from 'react';
import { Linking, StatusBar, StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import SimpleToast from 'react-native-simple-toast';
import VersionInfo from 'react-native-version-info';
import { getSafeState } from 'store';

export default ({navigation}: {navigation: AboutNavigation}) => {
  useLockedPortrait(navigation);
  const [pressed, setPressed] = useState(0);
  return (
    <Background>
      <>
        <StatusBar backgroundColor="black" />
        <Separator height={50} />
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => {
              if (pressed < 4) {
                setPressed(pressed + 1);
              } else {
                Clipboard.setString(JSON.stringify(getSafeState()));
                SimpleToast.show(
                  'Debug Mode : The Application state has been copied to the clipboard. Contact our team to help you debug.',
                  SimpleToast.LONG,
                );
              }
            }}>
            <Text style={styles.title}>
	            Blurtkey v{VersionInfo.appVersion} (Beta)
            </Text>
          </TouchableOpacity>
          <Text style={styles.text}>
            Blurtkey was forked by{' '}
            <Text
              style={styles.link}
              onPress={() => {
                Linking.openURL('https://blurt.blog/@eastmael');
              }}>
              @eastmael
            </Text>{' '}
            originally developed by{' '}
            <Text
              style={styles.link}
              onPress={() => {
                Linking.openURL('https://peakd.com/@stoodkev');
              }}>
              @stoodkev
            </Text>{' '}
            and designed by{' '}
            <Text
              style={styles.link}
              onPress={() => {
                Linking.openURL('https://peakd.com/@nateaguila');
              }}>
              @nateaguila
            </Text>
            .
            Images provided by{' '}
            <Text
              style={styles.link}
              onPress={() => {
                Linking.openURL('https://blurt.blog/@megadrive');
              }}>
              @megadrive
            </Text>
            .
          </Text>
          <Separator height={20} />
          <Text style={styles.text}>
            It originated from Hive Keychain, a browser extension imagined by the
            Splinterlands' founders{' '}
            <Text
              style={styles.link}
              onPress={() => {
                Linking.openURL('https://peakd.com/@yabapmatt');
              }}>
              @yabapmatt
            </Text>{' '}
            and{' '}
            <Text
              style={styles.link}
              onPress={() => {
                Linking.openURL('https://peakd.com/@aggroed');
              }}>
              @aggroed
            </Text>
            .
          </Text>
          <Separator height={20} />
          <Text style={styles.text}>
            Since the application is still in Beta, we count on you to report
            bugs, layout issues and improvement ideas on our{' '}
            <Text
              style={styles.link}
              onPress={() => {
                Linking.openURL(
                  'https://gitlab.com/blurt/openblurt/blurtopian/blurtkey-mobile',
                );
              }}>
              Gitlab.
            </Text>
          </Text>
        </View>
      </>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {marginVertical: 60, marginHorizontal: 20},
  link: {color: 'lightgrey'},
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    marginBottom: 40,
  },
  text: {color: 'white', fontSize: 16, textAlign: 'justify'},
});
