import Blurt from 'assets/wallet/icon_blurt.svg';
import Hp from 'assets/wallet/icon_hp.svg';
import Savings from 'assets/wallet/icon_savings.svg';
import AccountValue from 'components/hive/AccountValue';
import TokenDisplay from 'components/hive/TokenDisplay';
import {
  Send, SendDelegation,
  SendDeposit,
  SendPowerDown,
  SendPowerUp,
  SendWithdraw
} from 'components/operations/OperationsButtons';
import Separator from 'components/ui/Separator';
import React, { useEffect } from 'react';
import { StyleSheet, useWindowDimensions, View } from 'react-native';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from 'store';
import { logScreenView } from 'utils/analytics';
import { toHP } from 'utils/format';
import { translate } from 'utils/localize';

enum Token {
  NONE,
  BLURT,
  BP,
  SAVINGS,
}
const Primary = ({user, prices, properties}: PropsFromRedux) => {
  const {width} = useWindowDimensions();
  useEffect(() => {
    logScreenView('WalletScreen');
  }, []);

  return (
    <View style={styles.container}>
      <Separator height={30} />
      <AccountValue
        account={user.account}
        prices={prices}
        properties={properties}
      />
      <Separator height={30} />

      <TokenDisplay
        color="#A3112A"
        name="BLURT"
        currency="BLURT"
        value={parseFloat(user.account.balance as string)}
        logo={<Blurt width={width / 15} />}
        price={prices.price_usd}
        buttons={[
          <Send key="send_blurt" currency="BLURT" />,
          <SendPowerUp key="pu" />,
          <View style={{width: 20}}></View>,
        ]}
      />
      <Separator height={20} />
      <TokenDisplay
        color="#AC4F00"
        name="BLURT POWER"
        currency="BP"
        value={toHP(user.account.vesting_shares as string, properties.globals)}
        incoming={toHP(
          user.account.received_vesting_shares as string,
          properties.globals,
        )}
        outgoing={toHP(
          user.account.delegated_vesting_shares as string,
          properties.globals,
        )}
        logo={<Hp width={width / 15} />}
	      price={prices.price_usd}
        buttons={[
          <SendDelegation key="del" />,
          <SendPowerDown key="pd" />,
          <View style={{width: 20}}></View>,
        ]}
      />
      <Separator height={20} />
      <TokenDisplay
        color="#7E8C9A"
        name={translate('common.savings').toUpperCase()}
        currency="BLURT"
        value={parseFloat(user.account.savings_balance as string)}
        logo={<Savings width={width / 15} />}
        buttons={[
          <SendWithdraw key="savings_withdraw" currency="BLURT" />,
          <SendDeposit key="savings_deposit" currency="BLURT" />,
          <View style={{width: 20}}></View>,
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {width: '100%', flex: 1},
  apr: {color: '#7E8C9A', fontSize: 14},
  aprValue: {color: '#3BB26E', fontSize: 14},
});

const mapStateToProps = (state: RootState) => {
  return {
    user: state.activeAccount,
    prices: state.currencyPrices,
    properties: state.properties,
  };
};
const connector = connect(mapStateToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
export default connector(Primary);
