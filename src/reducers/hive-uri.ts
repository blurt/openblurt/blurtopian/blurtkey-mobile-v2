import { Operation } from '@beblurt/dblurt';
import { ActionPayload } from 'actions/interfaces';
import { HiveURIActionTypes } from 'actions/types';

export default (
  state: {operation?: Operation} = {},
  {type, payload}: ActionPayload<Operation | undefined>,
) => {
  switch (type) {
    case HiveURIActionTypes.SAVE_OPERATION:
      return {operation: payload};
    case HiveURIActionTypes.FORGET_OPERATION:
      return {};
    default:
      return state;
  }
};
