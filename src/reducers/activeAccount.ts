import { ExtendedAccount } from '@blurtopian/dblurt';
import { AccountKeys, ActionPayload, ActiveAccount } from 'actions/interfaces';
import {
  ACTIVE_ACCOUNT, FORGET_ACCOUNT,
  FORGET_ACCOUNTS
} from 'actions/types';

const activeAccountReducer = (
  state: ActiveAccount = {
    account: {} as ExtendedAccount,
    keys: {} as AccountKeys,
  },
  {type, payload}: ActionPayload<any>,
): ActiveAccount => {
  switch (type) {
    case ACTIVE_ACCOUNT:
      return {...state, ...payload};
    case FORGET_ACCOUNT:
    case FORGET_ACCOUNTS:
      return {account: {} as ExtendedAccount, keys: {}};
    default:
      return state;
  }
};

export default activeAccountReducer;
