import { loadAccount } from 'actions/index';
import Savings from 'assets/wallet/icon_savings.svg';
import ActiveOperationButton from 'components/form/ActiveOperationButton';
import CustomPicker from 'components/form/CustomPicker';
import OperationInput from 'components/form/OperationInput';
import Separator from 'components/ui/Separator';
import React, { useState } from 'react';
import {
  Alert, Keyboard,
  StyleSheet,
  Text,
  useWindowDimensions,
  View
} from 'react-native';
import Toast from 'react-native-simple-toast';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from 'store';
import { depositToSavings, withdrawFromSavings } from 'utils/blurt';
import { calculateTxFee } from 'utils/blurtUtils';
import { Dimensions } from 'utils/common.types';
import { getCurrencyProperties } from 'utils/hiveReact';
import { translate } from 'utils/localize';
import { goBack } from 'utils/navigation';
import Balance from './Balance';
import Operation from './Operation';
import SavingsBalance from './SavingsBalance';

type Props = PropsFromRedux & {currency: string; operation: SavingsOperations};
const Convert = ({user, loadAccount, currency: c, operation, properties}: Props) => {
  const [amount, setAmount] = useState('');
  const [loading, setLoading] = useState(false);
  const [currency, setCurrency] = useState(c);

  const onSavings = async () => {
    Keyboard.dismiss();
    setLoading(true);
    try {
      const operationData: any = getOperation(operation);
      if (operation === SavingsOperations.deposit) {
        await depositToSavings(user.keys.active, operationData);
      } else {
        await withdrawFromSavings(user.keys.active, operationData);
      }
      loadAccount(user.account.name, true);
      goBack();
      if (operation === SavingsOperations.deposit) {
        Toast.show(
          translate('toast.savings_deposit_success', {
            amount: `${(+amount).toFixed(3)} ${currency}`,
          }),
          Toast.LONG,
        );
      } else {
        Toast.show(
          translate('toast.savings_withdraw_success', {
            amount: `${(+amount).toFixed(3)} ${currency}`,
          }),
          Toast.LONG,
        );
      }
    } catch (e) {
      Toast.show(`Error : ${(e as any).message}`, Toast.LONG);
    } finally {
      setLoading(false);
    }
  };

  const getOperation = (operation: SavingsOperations) => {
    let operationData = {};
    if (operation === SavingsOperations.deposit) {
      operationData = {
        request_id: Date.now(),
        from: user.name,
        to: user.name,
        amount: `${(+amount).toFixed(3)} ${currency}`,
        memo: '',
      }
    } else {
      operationData = {
        request_id: Date.now(),
        from: user.name,
        to: user.name,
        amount: `${(+amount).toFixed(3)} ${currency}`,
        memo: '',
      }
    }
    return operationData;
  }

  const confirmTxFeeDialog = async () => {
    Keyboard.dismiss();
    const operation = getOperation();
    let fee = calculateTxFee({operation, properties});

    Alert.alert(
      "Transaction Fee",
      `This operation will cost ${fee} BLURT.`,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: onSavings
        }
      ]
    );
  }
  const {color} = getCurrencyProperties(currency);
  const styles = getDimensionedStyles(color, useWindowDimensions());
  return (
    <Operation
      logo={<Savings />}
      title={translate(`wallet.operations.savings.${operation}`)}>
      <>
        <View style={styles.container}>
          <CustomPicker
            list={['BLURT']}
            selectedValue={currency}
            onSelected={setCurrency}
            prompt={translate('wallet.operations.savings.prompt')}
            style={styles.picker}
            dropdownIconColor="white"
            iosTextStyle={styles.iosPickerText}
          />
        </View>
        {operation === SavingsOperations.deposit ? (
          <Balance
            currency={currency}
            account={user.account}
            setMax={(value: string) => {
              setAmount(value);
            }}
          />
        ) : (
          <SavingsBalance
            currency={currency}
            account={user.account}
            setMax={(value: string) => {
              setAmount(value);
            }}
          />
        )}
        <Separator />
        <Text style={styles.disclaimer}>
          {translate(`wallet.operations.savings.disclaimer`, {currency})}
        </Text>
        <Separator />
        <OperationInput
          placeholder={'0.000'}
          keyboardType="numeric"
          rightIcon={<Text style={styles.currency}>{currency}</Text>}
          textAlign="right"
          value={amount}
          onChangeText={setAmount}
        />
        <Separator height={50} />
        <ActiveOperationButton
          title={translate(
            `wallet.operations.savings.${operation}_button`,
          ).toUpperCase()}
          onPress={confirmTxFeeDialog}
          style={styles.button}
          isLoading={loading}
        />
        <Separator />
      </>
    </Operation>
  );
};

const getDimensionedStyles = (color: string, {width, height}: Dimensions) =>
  StyleSheet.create({
    button: {backgroundColor: '#68A0B4'},
    currency: {fontWeight: 'bold', fontSize: 18, color},
    disclaimer: {textAlign: 'justify'},
    container: {
      display: 'flex',
      flexDirection: 'row',
      backgroundColor: '#7E8C9A',
      borderRadius: height / 30,
      marginVertical: height / 30,
      alignContent: 'center',
      justifyContent: 'center',
      minHeight: 50,
    },
    picker: {
      width: '80%',
      color: 'white',
      alignContent: 'center',
    },
    iosPickerText: {color: 'white'},
  });

export enum SavingsOperations {
  deposit = 'deposit',
  withdraw = 'withdraw',
}

const connector = connect(
  (state: RootState) => {
    return {
      user: state.activeAccount,
      properties: state.properties,
    };
  },
  {
    loadAccount,
  },
);
type PropsFromRedux = ConnectedProps<typeof connector>;
export default connector(Convert);
