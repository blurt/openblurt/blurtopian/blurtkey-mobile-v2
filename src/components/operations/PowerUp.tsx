import { loadAccount } from 'actions/index';
import Hp from 'assets/wallet/icon_hp.svg';
import AccountLogoDark from 'assets/wallet/icon_username_dark.svg';
import ActiveOperationButton from 'components/form/ActiveOperationButton';
import OperationInput from 'components/form/OperationInput';
import Separator from 'components/ui/Separator';
import React, { useState } from 'react';
import { Keyboard, StyleSheet, Text, Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from 'store';
import { powerUp } from 'utils/blurt';
import { getCurrencyProperties } from 'utils/hiveReact';
import { sanitizeAmount, sanitizeUsername } from 'utils/hiveUtils';
import {calculateTxFee} from 'utils/blurtUtils';
import { translate } from 'utils/localize';
import { goBack } from 'utils/navigation';
import Balance from './Balance';
import Operation from './Operation';

type Props = PropsFromRedux & {currency?: string};

const PowerUp = ({currency = 'BLURT', user, loadAccount, properties}: Props) => {
  const [to, setTo] = useState(user.account.name);
  const [amount, setAmount] = useState('');
  const [loading, setLoading] = useState(false);

  const onPowerUp = async () => {
    Keyboard.dismiss();
    setLoading(true);

    try {
      const operation = getOperation();
      await powerUp(user.keys.active, operation);
      loadAccount(user.account.name, true);
      goBack();
      Toast.show(translate('toast.powerup_success'), Toast.LONG);
    } catch (e) {
      Toast.show(`Error: ${(e as any).message}`, Toast.LONG);
    } finally {
      setLoading(false);
    }
  };

  const getOperation = () => {
    return {
      amount: sanitizeAmount(amount, currency),
      to: sanitizeUsername(to),
      from: user.account.name,
    };
  }

  const confirmTxFeeDialog = async () => {
    Keyboard.dismiss();
    const operation = getOperation();
    let fee = calculateTxFee({operation, properties});

    Alert.alert(
      "Transaction Fee",
      `This operation will cost ${fee} BLURT.`,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: onPowerUp
        }
      ]
    );
  }

  const {color} = getCurrencyProperties(currency);
  const styles = getDimensionedStyles(color);
  return (
    <Operation
      logo={<Hp />}
      title={translate('wallet.operations.powerup.title')}>
      <>
        <Separator />
        <Balance
          currency={currency}
          account={user.account}
          setMax={(value: string) => {
            setAmount(value);
          }}
        />

        <Separator />
        <OperationInput
          placeholder={translate('common.username').toUpperCase()}
          leftIcon={<AccountLogoDark />}
          autoCapitalize="none"
          value={to}
          onChangeText={setTo}
        />
        <Separator />
        <OperationInput
          placeholder={'0.000'}
          keyboardType="decimal-pad"
          rightIcon={<Text style={styles.currency}>{currency}</Text>}
          textAlign="right"
          value={amount}
          onChangeText={setAmount}
        />

        <Separator height={40} />
        <ActiveOperationButton
          title={translate('common.send')}
          onPress={confirmTxFeeDialog}
          style={styles.button}
          isLoading={loading}
        />
      </>
    </Operation>
  );
};

const getDimensionedStyles = (color: string) =>
  StyleSheet.create({
    button: {backgroundColor: '#68A0B4'},
    currency: {fontWeight: 'bold', fontSize: 18, color},
  });

const connector = connect(
  (state: RootState) => {
    return {
      user: state.activeAccount,
      properties: state.properties,
    };
  },
  {loadAccount},
);
type PropsFromRedux = ConnectedProps<typeof connector>;
export default connector(PowerUp);
