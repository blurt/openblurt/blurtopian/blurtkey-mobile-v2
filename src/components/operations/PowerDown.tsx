import { loadAccount } from 'actions/index';
import Hp from 'assets/wallet/icon_hp_dark.svg';
import moment from 'moment';
import ActiveOperationButton from 'components/form/ActiveOperationButton';
import OperationInput from 'components/form/OperationInput';
import Separator from 'components/ui/Separator';
import React, { useState } from 'react';
import { Keyboard, StyleSheet, Text, Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from 'store';
import { powerDown } from 'utils/blurt';
import { fromHP, toHP, withCommas } from 'utils/format';
import { getCurrencyProperties } from 'utils/hiveReact';
import { sanitizeAmount } from 'utils/hiveUtils';
import {calculateTxFee} from 'utils/blurtUtils';
import { translate } from 'utils/localize';
import { goBack } from 'utils/navigation';
import Balance from './Balance';
import Operation from './Operation';

type Props = PropsFromRedux & {currency?: string};
const PowerDown = ({currency = 'BP', user, loadAccount, properties}: Props) => {
  const [amount, setAmount] = useState('');
  const [loading, setLoading] = useState(false);

  const renderPDIndicator = () => {
    if (parseFloat(user.account.to_withdraw as string) !== 0) {
      return (
        <Text>
          <Text style={styles.bold}>Current power down : </Text>
          {`${withCommas(
            toHP(user.account.withdrawn as string, properties.globals) /
              1000000 +
              '',
            1,
          )} / ${withCommas(
            toHP(user.account.to_withdraw as string, properties.globals) /
              1000000 +
              '',
            1,
          )} BP`}
        </Text>
      );
    } else {
      return null;
    }
  };
  const renderNextPDSchedule = () => {
    if (parseFloat(user.account.to_withdraw) !== 0) {
      let date = user.account.next_vesting_withdrawal;
      if (date && /^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d$/.test(date)) {
        date = date + 'Z'; // Firefox really wants this Z (Zulu)
      }
      let withdraw_date = new Date(user.account.next_vesting_withdrawal);

      return (
        <Text>
          {translate('wallet.operations.powerdown.next_power_down_is_scheduled_to_happen') + ' ' + moment(withdraw_date).fromNow()}
        </Text>
      );
    } else {
      return null;
    }
  };

  const onPowerDown = async () => {
    setLoading(true);
    Keyboard.dismiss();

    try {
      const operation = getOperation();
      await powerDown(user.keys.active!, operation);
      loadAccount(user.account.name, true);
      goBack();
      if (parseFloat(amount.replace(',', '.')) !== 0) {
        Toast.show(translate('toast.powerdown_success'), Toast.LONG);
      } else {
        Toast.show(translate('toast.stop_powerdown_success'), Toast.LONG);
      }
    } catch (e) {
      Toast.show(`Error : ${(e as any).message}`, Toast.LONG);
    } finally {
      setLoading(false);
    }
  };
  
  const getOperation = () => {
    return {
      vesting_shares: sanitizeAmount(
        fromHP(sanitizeAmount(amount), properties.globals!).toString(),
        'VESTS',
        6,
      ),
      account: user.account.name,
    };
  }

  const confirmTxFeeDialog = async () => {
    Keyboard.dismiss();
    const operation = getOperation();
    let fee = calculateTxFee({operation, properties});

    Alert.alert(
      "Transaction Fee",
      `This operation will cost ${fee} BLURT.`,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: onPowerDown
        }
      ]
    );
  }

  const {color} = getCurrencyProperties(currency);
  const styles = getDimensionedStyles(color);
  return (
    <Operation
      logo={<Hp />}
      title={translate('wallet.operations.powerdown.title')}>
      <>
        <Separator />
        <Balance
          currency={currency}
          account={user.account}
          pd
          globalProperties={properties.globals}
          setMax={(value: string) => {
            setAmount(value);
          }}
        />

        <Separator />
        {renderPDIndicator()}
        <Separator />

        <OperationInput
          placeholder={'0.000'}
          keyboardType="decimal-pad"
          rightIcon={<Text style={styles.currency}>{currency}</Text>}
          textAlign="right"
          value={amount}
          onChangeText={setAmount}
        />

        <Separator />
        {renderNextPDSchedule()}

        <Separator height={40} />
        <ActiveOperationButton
          title={translate('common.send')}
          onPress={confirmTxFeeDialog}
          style={styles.button}
          isLoading={loading}
        />
      </>
    </Operation>
  );
};

const getDimensionedStyles = (color: string) =>
  StyleSheet.create({
    button: {backgroundColor: '#68A0B4'},
    currency: {fontWeight: 'bold', fontSize: 18, color},
    bold: {fontWeight: 'bold'},
  });

const connector = connect(
  (state: RootState) => {
    return {
      user: state.activeAccount,
      properties: state.properties,
    };
  },
  {loadAccount},
);
type PropsFromRedux = ConnectedProps<typeof connector>;

export default connector(PowerDown);
