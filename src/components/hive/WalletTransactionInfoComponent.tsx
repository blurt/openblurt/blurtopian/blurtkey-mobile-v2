import { ActiveAccount } from 'actions/interfaces';
import React from 'react';
import { View } from 'react-native';
import { connect, ConnectedProps } from 'react-redux';
import {
  ClaimAccount,
  ClaimReward, CreateAccount,
  CreateClaimedAccount,
  Delegation,
  DepositSavings, PowerDown,
  PowerUp, StartWithdrawSavings,
  Transaction,
  Transfer as TransferInterface,
  WithdrawSavings
} from 'src/interfaces/transaction.interface';
import { RootState } from 'store';
import ClaimAccountTransactionComponent from './ClaimAccountTransactionComponent';
import ClaimRewardsTransactionComponent from './ClaimRewardsTransactionComponent';
import CreateAccountTransactionComponent from './CreateAccountTransactionComponent';
import CreateClaimedAccountTransactionComponent from './CreateClaimedAccountTransactionComponent';
import DelegationTransactionComponent from './DelegationTransactionComponent';
import DepositSavingsTransactionComponent from './DepositSavingsTransactionComponent';
import FillWithdrawSavingsTransactionComponent from './FillWithdrawSavingsTransactionComponent';
import PowerDownTransactionComponent from './PowerDownTransactionComponent';
import PowerUpTransactionComponent from './PowerUpTransactionComponent';
import Transfer from './Transfer';
import WithdrawSavingsTransactionComponent from './WithdrawSavingsTransactionComponent';

type Props = {
  user: ActiveAccount;
  transaction: Transaction;
  token?: boolean;
  locale: string;
};

const WalletTransactionInfo = ({
  transaction,
  user,
  locale,
  token = false,
}: Props & PropsFromRedux) => {
  const getTransactionContent = () => {
    switch (transaction.type) {
      case 'transfer':
        return (
          <Transfer
            transaction={transaction as TransferInterface}
            user={user}
            locale={locale}
            useIcon={true}
          />
        );

      case 'claim_reward_balance':
        return (
          <ClaimRewardsTransactionComponent
            transaction={transaction as ClaimReward}
            user={user}
            locale={locale}
            useIcon={true}
          />
        );

      case 'delegate_vesting_shares':
        return (
          <DelegationTransactionComponent
            transaction={transaction as Delegation}
            user={user}
            locale={locale}
            useIcon={true}
          />
        );

      case 'claim_account':
        return (
          <ClaimAccountTransactionComponent
            transaction={transaction as ClaimAccount}
            user={user}
            locale={locale}
            useIcon={true}
          />
        );

      case 'savings': {
        switch (transaction.subType) {
          case 'transfer_to_savings':
            return (
              <DepositSavingsTransactionComponent
                transaction={transaction as DepositSavings}
                user={user}
                locale={locale}
                useIcon={true}
              />
            );
          case 'transfer_from_savings':
            return (
              <WithdrawSavingsTransactionComponent
                transaction={transaction as WithdrawSavings}
                user={user}
                locale={locale}
                useIcon={true}
              />
            );
          case 'fill_transfer_from_savings':
            return (
              <FillWithdrawSavingsTransactionComponent
                transaction={transaction as StartWithdrawSavings}
                user={user}
                locale={locale}
                useIcon={true}
              />
            );
        }
      }
      case 'power_up_down': {
        switch (transaction.subType) {
          case 'withdraw_vesting':
            return (
              <PowerDownTransactionComponent
                transaction={transaction as PowerDown}
                user={user}
                locale={locale}
                useIcon={true}
              />
            );
          case 'transfer_to_vesting':
            return (
              <PowerUpTransactionComponent
                transaction={transaction as PowerUp}
                user={user}
                locale={locale}
                useIcon={true}
              />
            );
        }
      }

      case 'account_create':
        return (
          <CreateAccountTransactionComponent
            transaction={transaction as CreateAccount}
            user={user}
            locale={locale}
            useIcon={true}
          />
        );

      case 'create_claimed_account':
        return (
          <CreateClaimedAccountTransactionComponent
            transaction={transaction as CreateClaimedAccount}
            user={user}
            locale={locale}
            useIcon={true}
          />
        );
    }
  };
  return <View>{getTransactionContent()}</View>;
};

const mapStateToProps = (state: RootState) => {
  return {};
};

const connector = connect(mapStateToProps, {});
type PropsFromRedux = ConnectedProps<typeof connector>;

export const WalletTransactionInfoComponent = connector(WalletTransactionInfo);
