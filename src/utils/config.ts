export const hiveEngine = {
  CHAIN_ID: 'ssc-mainnet-hive',
};

export const hiveConfig = {
  CREATE_ACCOUNT_URL: 'https://signup.hive.io/',
};

export const HASConfig = {
  protocol: 'has://',
  auth_req: 'has://auth_req/',
  socket: 'wss://hive-auth.arcange.eu',
};

export type DApp = {
  name: string;
  description: string;
  icon: string;
  url: string;
  appendUsername?: boolean;
  categories: string[];
};

export const BrowserConfig = {
  HOMEPAGE_URL: 'about:blank',
  FOOTER_HEIGHT: 40,
  HEADER_HEIGHT: 45,
  HomeTab: {
    categories: [
      {title: 'finance', color: '#2A5320', logo: ''},
      {title: 'social', color: '#215858', logo: ''},
      // {title: 'video', color: '#2A435C', logo: ''},
      {title: 'explorer', color: '#6A3434', logo: ''},
      {title: 'tool', color: '#353E3E', logo: ''},
    ],
    dApps: [
      {
        name: 'Blurt Wallet',
        description: 'Official wallet for Blurt',
        icon: 'https://blocks.blurtwallet.com/img/blurt-logo.294f8e1c.png',
        url: 'https://blurtwallet.com/@',
        appendUsername: true,
        categories: ['finance'],
      },
      {
        name: 'Blurt.blog',
        description: 'Community interface for Blurt',
        icon: 'https://blocks.blurtwallet.com/img/blurt-logo.294f8e1c.png',
        url: 'https://blurt.blog',
        categories: ['social'],
      },
      {
        name: 'BeBlurt',
        description: 'Community interface by @beblurt',
        icon: 'https://beblurt.com/assets/icons/icon-128x128.png',
        url: 'https://beblurt.com/',
        categories: ['social'],
      },
      {
        name: 'Blurt.one',
        description: 'Community interface by @blurt.one',
        icon: 'https://blocks.blurtwallet.com/img/blurt-logo.294f8e1c.png',
        url: 'https://blurt.one/',
        categories: ['social'],
      },
      {
        name: 'Blurt PWA',
        description: 'PWA for Blurt',
        icon: 'https://blocks.blurtwallet.com/img/blurt-logo.294f8e1c.png',
        url: 'https://pwa.blurt.blog/',
        categories: ['social'],
      },
      {
        name: 'Blurt Explorer',
        description: 'Official Block Explorer',
        icon: 'https://blocks.blurtwallet.com/img/blurt-logo.294f8e1c.png',
        url: 'https://ecosynthesizer.com/blurt/@',
        appendUsername: true,
        categories: ['explorer'],
      },
      {
        name: 'Ecosynthesizer Explorer',
        description: 'Block Explorer by @symbionts',
        icon: 'https://ecosynthesizer.com/view/img/ECS-LOGO-2021.png',
        url: 'https://ecosynthesizer.com/blurt/@',
        appendUsername: true,
        categories: ['explorer'],
      },
      {
        name: 'Blurt Now',
        description: 'Track & Analyze Your Blurt Blockchain Account',
        icon: 'https://blurtopian.com/assets/img/logo-blurtopian.png',
        url: 'https://blurt-now.com',
        categories: ['tool'],
      },
      {
        name: 'Blurt BI (Business Intelligence)',
        description: 'For Your Blurt Business Intelligence Needs',
        icon: 'https://blurtopian.com/assets/img/logo-blurtopian.png',
        url: 'https://bi.blurt-now.com/',
        categories: ['tool'],
      }
    ],
  },
};

export const KeychainConfig = {
  NO_USERNAME_TYPES: [
    'delegation',
    'witnessVote',
    'proxy',
    'custom',
    'signBuffer',
    'transfer',
  ],
};
