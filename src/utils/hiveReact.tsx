import React from 'react';
import { ExtendedAccount } from '@beblurt/dblurt';
import Blurt from 'assets/wallet/icon_blurt.svg';
import Bp from 'assets/wallet/icon_hp.svg';

export const getCurrencyProperties = (
  currency: string,
  account?: ExtendedAccount,
) => {
  let color, value, logo;
  switch (currency) {
    case 'BLURT':
      color = '#A3112A';
      logo = <Blurt />;
      value = account ? account.balance : null;
      break;
    default:
      color = '#AC4F00';
      value = account ? account.vesting_shares : null;
      logo = <Bp />;
      break;
  }
  return {currency, color, value, logo};
};
