import { DynamicGlobalProperties, ExtendedAccount } from '@blurtopian/dblurt';
import { CurrencyPrices } from 'actions/interfaces';
import blurt_price from 'api/blurt_price';
import ionomy from 'api/ionomy';
import { toHP } from 'utils/format';

export const getPrice = async () => {
  return (await ionomy.get('/api/v1/public/market-summary?market=btc-blurt'));
};

export const getBlurtPrice = async () => {
  return (await blurt_price.get('/price_info')).data;
};
export const getAccountValue = (
  {
    balance,
    vesting_shares,
    savings_balance,
  }: ExtendedAccount,
  {price_btc, price_usd}: CurrencyPrices,
  props: DynamicGlobalProperties,
) => {
  if (!price_btc || !price_usd) return 0;
  return (
    (toHP(vesting_shares as string, props) +
      parseFloat(balance as string) +
      parseFloat(savings_balance as string)) *
      price_usd
  ).toFixed(3);
};
