import { DynamicGlobalProperties, utils as dBlurtUtils } from '@blurtopian/dblurt';
import { decodeMemo } from 'components/bridge';
import {
  ClaimAccount,
  ClaimReward, CreateAccount,
  CreateClaimedAccount,
  Delegation,
  DepositSavings, PowerDown,
  PowerUp, StartWithdrawSavings,
  Transaction,
  Transfer,
  WithdrawSavings
} from 'src/interfaces/transaction.interface';
import { getBClient } from './blurt';
import { toHP } from './format';
import { translate } from './localize';

export const MINIMUM_FETCHED_TRANSACTIONS = 1;
export const NB_TRANSACTION_FETCHED = 200;
export const HAS_IN_OUT_TRANSACTIONS = ['transfer', 'delegate_vesting_shares'];
export const TRANSFER_TYPE_TRANSACTIONS = [
  'transfer',
  'fill_reccurent_transfer',
  'recurrent_transfer',
];

export const CONVERT_TYPE_TRANSACTIONS = [
  'convert',
  'fill_collateralized_convert_request',
  'fill_convert_request',
  'collateralized_convert',
];

const getAccountTransactions = async (
  accountName: string,
  start: number | null,
  globals: DynamicGlobalProperties,
  memoKey?: string,
): Promise<[Transaction[], number]> => {

  try {
    const op = dBlurtUtils.operationOrders;
    const operationsBitmask = dBlurtUtils.makeBitMaskFilter([
      op.transfer,
      op.claim_reward_balance,
      op.delegate_vesting_shares,
      op.transfer_to_vesting,
      op.withdraw_vesting,
      op.transfer_to_savings,
      op.transfer_from_savings,
      op.fill_transfer_from_savings,
      op.claim_account,
      op.account_create,
      op.create_claimed_account,
    ]) as [number, number];

    let limit = Math.min(start, NB_TRANSACTION_FETCHED);

    if (limit <= 0) return [[], 0];

    const transactionsFromBlockchain = await getBClient().database.getAccountHistory(
      accountName,
      start,
      limit,
      operationsBitmask,
    );

    const transactions = [];
    for (let i = 0; i < transactionsFromBlockchain.length; i++) {
      let e = transactionsFromBlockchain[i];
      let specificTransaction = null;

      switch (e[1].op[0]) {
        case 'transfer': {
          specificTransaction = e[1].op[1] as Transfer;
          specificTransaction = decodeMemoIfNeeded(
            specificTransaction,
            memoKey,
          );
          break;
        }
        case 'claim_reward_balance': {
          specificTransaction = e[1].op[1] as ClaimReward;
          specificTransaction.blurt = e[1].op[1].reward_blurt;
          specificTransaction.bp = `${toHP(
            e[1].op[1].reward_vests,
            globals,
          ).toFixed(3)} BP`;
          break;
        }
        case 'delegate_vesting_shares': {
          specificTransaction = e[1].op[1] as Delegation;
          specificTransaction.amount = `${toHP(
            e[1].op[1].vesting_shares,
            globals,
          ).toFixed(3)} BP`;
          break;
        }
        case 'transfer_to_vesting': {
          specificTransaction = e[1].op[1] as PowerUp;
          specificTransaction.type = 'power_up_down';
          specificTransaction.subType = 'transfer_to_vesting';
          break;
        }
        case 'withdraw_vesting': {
          specificTransaction = e[1].op[1] as PowerDown;
          specificTransaction.type = 'power_up_down';
          specificTransaction.subType = 'withdraw_vesting';
          specificTransaction.amount = `${toHP(
            e[1].op[1].vesting_shares,
            globals,
          ).toFixed(3)} BP`;
          break;
        }
        case 'transfer_to_savings': {
          specificTransaction = e[1].op[1] as DepositSavings;
          specificTransaction.type = 'savings';
          specificTransaction.subType = 'transfer_to_savings';
          break;
        }
        case 'transfer_from_savings': {
          specificTransaction = e[1].op[1] as StartWithdrawSavings;
          specificTransaction.type = 'savings';
          specificTransaction.subType = 'transfer_from_savings';
          break;
        }
        case 'fill_transfer_from_savings': {
          specificTransaction = e[1].op[1] as WithdrawSavings;
          specificTransaction.type = 'savings';
          specificTransaction.subType = 'fill_transfer_from_savings';
          break;
        }
        case 'claim_account': {
          specificTransaction = e[1].op[1] as ClaimAccount;
          break;
        }
        case 'create_claimed_account': {
          specificTransaction = e[1].op[1] as CreateClaimedAccount;
          break;
        }
        case 'account_create': {
          specificTransaction = e[1].op[1] as CreateAccount;
          break;
        }
      }

      if (!specificTransaction) {
        continue;
      }

      const tr: Transaction = {
        ...specificTransaction,
        type: specificTransaction!.type ?? e[1].op[0],
        timestamp: e[1].timestamp,
        key: `${accountName}!${e[0]}`,
        index: e[0],
        txId: e[1].trx_id,
        blockNumber: e[1].block,
        url:
          e[1].trx_id === '0000000000000000000000000000000000000000'
            ? `https://blocks.blurtwallet.com/#/b/${e[1].block}#${e[1].trx_id}`
            : `https://blocks.blurtwallet.com/#/tx/${e[1].trx_id}`,
        last: false,
        lastFetched: false,
      };

      transactions.push(tr);

    }

    transactions.sort(
      (a, b) =>
        new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime(),
    );

    if (start - NB_TRANSACTION_FETCHED < 0 && transactions.length > 1) {
      transactions[transactions.length - 1].last = true;
    }

    if (
      start &&
      Math.min(NB_TRANSACTION_FETCHED, start) !== NB_TRANSACTION_FETCHED &&
      transactions.length > 1
    ) {
      transactions[transactions.length - 1].lastFetched = true;
    }
    return [transactions, start];
  } catch (e) {
    if (e.message && e.message === 'Request Timeout') {
      return await getAccountTransactions(accountName, start, globals, memoKey);
    } else {
      return await getAccountTransactions(
        accountName,
        (e as any).jse_info.stack[0].data.sequence - 1,
        globals,
        memoKey,
      );
    }
  }
};

const decodeMemoIfNeeded = (transfer: Transfer, memoKey: string) => {
  const {memo} = transfer;
  if (memo[0] === '#') {
    if (memoKey) {
      decodeMemo(memoKey, memo)
        .then((decoded) => {
          transfer.memo = decoded;
          return transfer;
        })
        .catch((e) => {
          console.log('Error while decoding memo: ', e);
        });
    } else {
      transfer.memo = translate('wallet.add_memo');
    }
  }
  return transfer;
};

const getLastTransaction = async (accountName: string) => {
  const op = dBlurtUtils.operationOrders;
  const allOp = Object.values(op);
  const allOperationsBitmask = dBlurtUtils.makeBitMaskFilter(allOp) as [
    number,
    number,
  ];
  const transactionsFromBlockchain = await getBClient().database.getAccountHistory(
    accountName,
    -1,
    1,
    allOperationsBitmask,
  );
  return transactionsFromBlockchain.length > 0
    ? transactionsFromBlockchain[0][0]
    : -1;
};

const TransactionUtils = {
  getAccountTransactions,
  getLastTransaction,
  decodeMemoIfNeeded,
};

export default TransactionUtils;
